#include <string>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include "Registrar.h"

using namespace std;
Registrar::Registrar() {
	regis = new Student[50000]; //Initialize the array to 50000 the max.
	regisIndex = 0; //Starting index is 0
}

// copy constructor
Registrar::Registrar(const Registrar& fva) {
	regisIndex = fva.regisIndex; //Updated to new index
	regis = new Student[50000]; //Create new array
	for (int i = 0; i < regisIndex; i++){
		regis[i] = fva.regis[i]; //Copy over values
	}
}
// assignment operator
Registrar& Registrar::operator = (const Registrar &fva) {
	regisIndex = fva.regisIndex; //Updated new idnex
	regis = new Student[50000]; //Create new array
	for (int i = 0; i < regisIndex; i++){ 
		regis[i] = fva.regis[i]; //Copy over values
	}
	return *this;
}

// destructor
Registrar::~Registrar() {
	delete[] regis; //Delete the newly created array.
}


// Load information from a text file with the given filename
// THIS FUNCTION IS COMPLETE
void Registrar::readTextfile(string filename) {
	ifstream myfile(filename);

	if (myfile.is_open()) {
		cout << "Successfully opened file " << filename << endl;
		string courseName;
		string cwid;
		char grade;
		while (myfile >> courseName >> cwid >> grade) {
			// cout << cwid << " " << grade << endl;
			addLine(courseName, cwid, grade);
		}
		myfile.close();
	}
	else
		throw invalid_argument("Could not open file " + filename);
}

// return Student object corresponding to a given CWID
// getStudent must throw an exception if cwid is invalid
Student& Registrar::getStudent(string cwid) const {
	for (int i = 0; i < regisIndex; i++){ //Loop through all students
		if (regis[i].getCWID() == cwid) //If the CWID matches
			return regis[i]; //Return the student
	}
	throw runtime_error("Could not find a student with that CWID.");
}

// process a line from the text file
void Registrar::addLine(string courseName, string cwid, char grade) {
	bool exists = false;
	for (int i = 0; i < regisIndex; i++){
		if (regis[i].getCWID() == cwid){ //If the cwid matches it exists in register
			exists = true; //Say that it exists
			break; //Break the loop
		}
	}

	if (!exists){
		Student student(cwid); //Create a new student
		student.addCourseGrade(courseName, grade); //Add course name/grade to the student.
		regis[regisIndex] = student; //Add new student to registrar
		regisIndex++; //Increase the index
	}else
	    getStudent(cwid).addCourseGrade(courseName, grade); //Add value to existing course grade

}

