#pragma once
#include <string>

using namespace std;

class Student {
public:
	Student(); // default constructor
	Student(const string &gcwid); // constructor with parameter
	void addCourseGrade(const string &courseName, char grade); // add course name and grade to student's record
	double getGPA(); // calculate and return GPA
	void printTranscript(); // print transcript - see Student.cpp for the format
	string getCWID(); // return the CWID of this student
private:
	string cwid;
	int transcriptIndex; //Index for the parallel array
	double gpa;
	int *grade; //Parallel Array for grade and course
	string *course; //Parallel Array for course and grade

};
