#include "Student.h"

#include <iostream>

Student::Student() {
	//No default constructor, CWID should always be supplied.
}

Student::Student(const string &gcwid) {
	cwid = gcwid;
	grade = new int[50];
	course = new string[50];
	transcriptIndex = 0;
}

string Student::getCWID() {
	return cwid;
}

void Student::addCourseGrade (const string &courseName, char g) {
	int val = 0;
	switch (toupper(g)){ //Converts to upper case and finds numerical value
	case 'A':
		val = 4;
		break;
	case 'B':
		val = 3;
		break;
	case 'C':
		val = 2;
		break;
	case 'D':
		val = 1;
		break;
	default:
		break;
	}
	course[transcriptIndex] = courseName; //Put the course name into array
	grade[transcriptIndex] = val; //Put the course grade into array
	transcriptIndex++; //Increase it to the next slot.

}


double Student::getGPA() {
	double total = 0;
	for (int i = 0; i < transcriptIndex; i++){ //Loop thjrough all the grades
		total += grade[i]; //Sum up all the grades.
	}
	return (total / (transcriptIndex)); //Calculate average based on how many grades as its an index.

}

// print transcript in this (sample) format:
// TRANSCRIPT FOR CWID=279750343
// CS 121		C
// CS 253		B
// CS 131		B
// GPA = 2.6667
void Student::printTranscript() {
	cout << "TRANSCRIPT FOR CWID=" << getCWID() << endl;
	for (int i = 0; i < transcriptIndex; i++){
		cout << course[i] << "     " << grade[i] << endl;
	}
	cout << "GPA = " << getGPA() << endl;

}

