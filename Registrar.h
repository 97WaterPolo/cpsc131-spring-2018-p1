#pragma once
#include <string>
#include <stdexcept>
#include "Student.h"

using namespace std;

class Registrar {
public:
	Registrar(const Registrar& fva);  // copy constructor
	Registrar& operator = (const Registrar& fva); // assignment operator
	~Registrar();      // destructor
	void readTextfile(string filename); // Load information from a text file with the given filename: THIS IS COMPLETE
	void addLine(string courseName, string cwid, char grade); // process a line from the text file
	Student& getStudent(string cwid) const; // return the Student object corresponding to a given CWID
	Registrar();
	// getStudent must throw an exception if cwid is invalid
	// add constructors, destructors, assignment operators if needed

private:
	Student *regis; //The array of students
	int regisIndex; //The index of students.

};
